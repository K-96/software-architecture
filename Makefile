#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

REGISTRY_HOST = registry.gitlab.com
REGISTRY_PATH = k-96/software-architecture/
LATEST = latest

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

# Build

CRUD_NAME = crud-rest-api
CRUD_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)$(CRUD_NAME)

pull-crud: ## Pull image with crud-rest-api service (tag=latest).
	docker pull "$(CRUD_IMAGE):$(LATEST)"
build-crud-first: ## Build image with crud-rest-api service. Example: $ make build-crud VERSION='0.1'
	docker build \
		--tag "$(CRUD_NAME):$(VERSION)" \
		--tag "$(CRUD_NAME):$(LATEST)" \
		--tag "$(CRUD_IMAGE):$(VERSION)" \
		--tag "$(CRUD_IMAGE):$(LATEST)" \
		-f ./docker/$(CRUD_NAME)/Dockerfile ./services/$(CRUD_NAME);
build-crud: ## Build image with crud-rest-api service with cache. Example: $ make build-crud VERSION='0.1'
	docker build \
		--cache-from "$(CRUD_IMAGE):$(LATEST)" \
		--tag "$(CRUD_NAME):$(VERSION)" \
		--tag "$(CRUD_NAME):$(LATEST)" \
		--tag "$(CRUD_IMAGE):$(VERSION)" \
		--tag "$(CRUD_IMAGE):$(LATEST)" \
		-f ./docker/$(CRUD_NAME)/Dockerfile ./services/$(CRUD_NAME);
push-crud: ## Push image with crud-rest-api service in registry. Example: $ make push-crud VERSION='0.1'
	docker login $(REGISTRY_HOST);
	docker push "$(CRUD_IMAGE):$(VERSION)";
	docker push "$(CRUD_IMAGE):$(LATEST)";
build-crud-xd:
	docker build \
		--tag "$(CRUD_NAME)-xd:$(LATEST)" \
		--tag "$(CRUD_IMAGE):$(LATEST)" \
		-f ./docker/$(CRUD_NAME)/XDEBUG.Dockerfile ./docker/$(CRUD_NAME);

CRUD_TESTS_NAME = crud-rest-api-tests
CRUD_TESTS_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)$(CRUD_TESTS_NAME)

build-crud-tests:
	docker build \
		--tag "$(CRUD_TESTS_NAME):$(VERSION)" \
		--tag "$(CRUD_TESTS_NAME):$(LATEST)" \
		--tag "$(CRUD_TESTS_IMAGE):$(VERSION)" \
		--tag "$(CRUD_TESTS_IMAGE):$(LATEST)" \
		-f ./docker/$(CRUD_NAME)/ACCEPTANCE_TESTS.Dockerfile ./tests/$(CRUD_NAME);
push-crud-tests:
	docker login $(REGISTRY_HOST);
	docker push "$(CRUD_TESTS_IMAGE):$(VERSION)";
	docker push "$(CRUD_TESTS_IMAGE):$(LATEST)";
# Deploy

K8S_NS = myapp
CRUD_FILES= -f ./k8s/crud-rest-api/deployment.yaml \
		-f ./k8s/crud-rest-api/service.yaml \
		-f ./k8s/crud-rest-api/config.yaml

create-namespace: ## Create namespace. K8S_NS - default as $(K8S_NS)
	@kubectl create namespace $(K8S_NS) || echo 'Namespaces "$(K8S_NS)" already exists'
set-namespace: create-namespace ## Set namespace
	kubectl config set-context --current --namespace=$(K8S_NS)
set-docker-config: set-namespace ## *Deprecated* Use Helm. Set docker config
	kubectl apply -f ./k8s/docker-secret.yaml
deploy-crud-dep: set-namespace ## *Deprecated* Use Helm. Deploy crud-rest-api service in k8s
	kubectl apply $(CRUD_FILES)
deploy-ingress-dep: set-namespace ## *Deprecated* Use Helm. Deploy ingress
	kubectl apply -f ./k8s/ingress.yaml
deploy-postgres: set-namespace ## *Deprecated* Use Helm. Deploy PostgreSQL
	kubectl apply -f ./k8s/postgres/config.yaml -f ./k8s/postgres/postgres.yaml
deploy-migrate: set-namespace ## *Deprecated* Use Helm. Run migrate
	kubectl apply -f ./k8s/crud-rest-api/config.yaml -f ./k8s/crud-rest-api/job-migrate.yaml
deploy: set-namespace set-docker-config deploy-crud-dep deploy-ingress-dep deploy-postgres ## *Deprecated* Use Helm. Set namespace myapp and deploy all
	@echo "Go to: http://arch.homework/otusapp/evgen"
	@echo "Run: \e[32mmake test\e[0m"

deleted-crud-dep: ## *Deprecated* Use Helm. Deleted all crud
	@kubectl delete $(CRUD_FILES) || echo "skip";
deleted: deleted-crud-dep ## *Deprecated* Use Helm. Deleted all apply
	@kubectl delete -f ./k8s/docker-secret.yaml || echo "skip";
	@kubectl delete -f ./k8s/ingress.yaml || echo "skip";
	@kubectl delete $(CRUD_FILES) || echo "skip";
	@kubectl delete -f ./k8s/postgres/config.yaml -f ./k8s/postgres/postgres.yaml || echo "skip";

HELM_RELEASE = $(K8S_NS)

install-dep-helm: ## Install dependencies Helm
	helm dependency update ./helm/crud-rest-api/
deploy-crud-prod: set-namespace ## Deploy prod crud-rest-api chart
	helm install --create-namespace --namespace $(K8S_NS) $(HELM_RELEASE) ./helm/$(CRUD_NAME)/ --atomic
try-cp-dev-values:
	cp -n ./helm/$(CRUD_NAME)-dev/example-values.yaml ./helm/$(CRUD_NAME)-dev/values.yaml
deploy-crud: set-namespace try-cp-dev-values ## Deploy crud-rest-api chart
	helm install \
		--create-namespace --namespace $(K8S_NS) $(HELM_RELEASE) \
		./helm/$(CRUD_NAME)/ -f ./helm/$(CRUD_NAME)-dev/values.yaml \
		--atomic
dry-run-crud: set-namespace try-cp-dev-values
	helm template $(HELM_RELEASE) ./helm/$(CRUD_NAME)/ -f ./helm/$(CRUD_NAME)-dev/values.yaml \
		| cat > ./helm/$(CRUD_NAME)-dev/template-debug.yaml

deleted-crud: set-namespace ## Deleted crud-rest-api chart
	helm delete $(HELM_RELEASE) || echo "skip";

deleted-all: set-namespace deleted deleted-crud ## Deleted !ALL!
	@kubectl delete all --all || echo "skip";
	@kubectl delete pvc --all || echo "skip";
	@kubectl delete pv --all || echo "skip";

# Monitoring

watch: ## Run watch kubectl
	watch kubectl get all --namespace $(K8S_NS)

deploy-monitoring: set-namespace ## Install prometheus and grafana
	helm install --create-namespace --namespace $(K8S_NS) prometheus stable/prometheus-operator -f ./helm/monitoring/prometheus.yaml --atomic;

deploy-ingress-nginx: set-namespace ## Run metrics nginx
	helm upgrade nginx stable/nginx-ingress -f ./helm/monitoring/nginx-ingress.yaml

forward-prometheus:
	kubectl port-forward service/prometheus-prometheus-oper-prometheus 9090;

forward-grafana:
	kubectl port-forward service/prometheus-grafana 9001:80;

deleted-monitoring:
	helm delete prometheus;
	helm delete nginx;

# Local

bind-host: ## Bind minikube ip to arch.homework host
	@echo "$$(minikube ip)	arch.homework" | sudo tee --append /etc/hosts

# Tests

fail-test:
	@echo "Run fail test:"
	@echo "\e[31m$$(curl http://arch.homework/otusapp/evgen/not-found --fail -s || echo "\e[32mFail! \e[0m")\e[0m"
test: fail-test ## Curl tests
	@echo "Run success tests:"
	@echo "\e[32m$$(curl http://arch.homework/otusapp/evgen/ --fail -s || echo "\e[31mFail! \e[0m")\e[0m"
	@echo "\e[32m$$(curl http://arch.homework/otusapp/evgen/version --fail -s || echo "\e[31mFail! \e[0m")\e[0m"
	@echo "\e[32m$$(curl http://arch.homework/otusapp/evgen/health --fail -s || echo "\e[31mFail! \e[0m")\e[0m"

test-crud: ## Tests crud-rest-api chart
	helm test myapp
	@kubectl logs -l "job-name=crud-rest-api-test-job" --tail=-1

stress-test:
	while true ; do ab -n 50 -c 3 http://arch.homework/otusapp/evgen/api/products; sleep 3 ; done
stress-test-404:
	while true ; do ab -n 50 -c 3 http://arch.homework/otusapp; sleep 3 ; done

# service.namespace.svc.cluster.local

deploy-apigetway-hw:
	@kubectl apply -f k8s/docker-secret.yaml || echo 'skip apply docker-secret'
	cd services/user-cabinet-service && make deploy
	cd services/auth-jwt-service && make install && make deploy
	cd services/api-getway-service && make install && make deploy
	minikube service aes-ambassador --url -n apigetway

delete-apigetway-hw:
	cd services/user-cabinet-service && make delete
	cd services/auth-jwt-service && make delete
	cd services/api-getway-service && make delete