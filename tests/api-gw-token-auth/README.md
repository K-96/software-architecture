```
evgen@evgen:/var/www/html/software-architecture$ make deploy-apigetway-hw 
...
make[1]: Leaving directory '/var/www/html/software-architecture/services/api-getway-service'
minikube service aes-ambassador --url -n apigetway
http://172.17.0.2:31515 # адрес который надо подставить в baseUrl
http://172.17.0.2:30573
evgen@evgen:/var/www/html/software-architecture$ 
```

```shell script
newman run api-gw-token-auth.postman_collection.json \
    --env-var "baseUrl=http://172.17.0.2:31515/otusapp/evgen"
```

```shell script
newman run api-gw-token-auth.postman_collection.json \
    --env-var "baseUrl=http://arch.homework/otusapp/evgen"
```