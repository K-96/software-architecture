# Архитектура программного обеспечения

### Введение

Сервис будет состоять из следующего ПО:

* Kubernetes
* Docker
* Helm
* Lumen (PHP Framework)
* PostgreSQL
* Redis
* Prometheus, Grafana
* Python
* Ambassador (api getway, bff)

>Список будет дополнятся

### Makefile

```bash
make help
```

##### Первый запуск

```bash
make deploy-crud
```

> Может не достучатся до gitlab registry (у меня лечилось перезапуском minkube).
> Видимо проблема с рабочим vpn, не резолвился домен.

##### Удалить все деплои и конфиги

```bash
make deleted-all
```

##### Тесты

###### cURL, проверка на жизнеспособность
```bash
make test
```

###### Newman (Postman), приемочные тесты на HTTP REST API для crud сервиса
```bash
make crud
```

##### Мониторинг

###### Мониторинг состояния сущностей в кубике.

```bash
make watch # watch kubectl get all -n myapp
make watch K8S_NS=some_namespace # watch kubectl get all -n some_namespace
```

###### Мониторинг USE, RED, Golden signals

В helm чарте crud сервиса есть ServiceMonitoring. Через values
включается/выключается.

```bash
make deploy-monitoring K8S_NS=monitoring
```
```bash
make forward-prometheus # http://localhost:9090
```
```bash
make forward-grafana # http://localhost:9001. Логин пароль: admin:prom-operator
```
