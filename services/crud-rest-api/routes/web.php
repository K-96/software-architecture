<?php

use Laravel\Lumen\Routing\Router;
use Triadev\PrometheusExporter\Controller\LumenController;

/** @var $router Router */

$router->get('/', function () {
    return response()->json([
        'message' => 'Hello! I am ' . env('HOSTNAME') . '.'
    ]);
});
$router->get('/version', function () {
    return response()->json(['version' => env('APP_VERSION')]);
});
$router->get('/health', function () {
    return response()->json(['status' => 'ok']);
});
$router->get('metrics', '\\' . LumenController::class . '@metrics');

$router->group([
    'prefix' => 'api',
    'middleware' => 'lpe.requestPerRoute',
], function () use ($router) {
    $router->get('products',  ['uses' => 'Api\ProductsController@index']);
    $router->get('products/{id}', ['uses' => 'Api\ProductsController@show']);
    $router->post('products', ['uses' => 'Api\ProductsController@store']);
    $router->put('products/{id}', ['uses' => 'Api\ProductsController@update']);
    $router->delete('products/{id}', ['uses' => 'Api\ProductsController@destroy']);
});
