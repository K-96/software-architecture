<?php

return [
    'adapter' => env('PROMETHEUS_ADAPTER', 'redis'),

    'namespace' => 'app',

    'namespace_http' => 'http',

    'redis' => [
        'host' => env('PROMETHEUS_REDIS_HOST', env('REDIS_HOST', '127.0.0.1')),
        'port' => (int) env('PROMETHEUS_REDIS_PORT', env('REDIS_PORT', 6379)),
        'password' => env('PROMETHEUS_REDIS_PASSWORD', env('REDIS_PASSWORD', null)),
        'timeout' => 0.1,  // in seconds
        'read_timeout' => 10, // in seconds
        'persistent_connections' => false,
    ],

    'push_gateway' => [
        'address' => env('PROMETHEUS_PUSH_GATEWAY_ADDRESS', 'localhost:9091')
    ],

    'buckets_per_route' => array_map(function (float $le): int {
        return (int) ($le * 1000); // делаем бакеты в миллисекундах
    }, [
        0.005, 0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5, 10.0,
    ]),
];
