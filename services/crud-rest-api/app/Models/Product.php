<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int         $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string      $name
 * @property string      $description
 * @property float       $price
 * @property int         $count
 * @method static find($id)
 */
class Product extends Model
{
}
