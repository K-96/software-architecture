<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(Product::all());
    }

    public function show($id)
    {
        return response()->json(Product::find($id));
    }

    public function store(Request $request)
    {
        $product = new Product;

        $product->name= $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->count= $request->input('count');

        $product->save();

        return response()->json($product);
    }

    public function update(Request $request, $id)
    {
        $product= Product::find($id);

        if ($request->has('name')) {
            $product->name= $request->input('name');
        }
        if ($request->has('description')) {
            $product->description= $request->input('description');
        }
        if ($request->has('price')) {
            $product->price= $request->input('price');
        }
        if ($request->has('count')) {
            $product->count= $request->input('count');
        }

        $product->save();

        return response()->json($product);
    }
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return response()->json(['message' => 'Product removed successfully!']);
    }
}
