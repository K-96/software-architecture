{{- define "crud-rest-api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.name" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.deployment" -}}
{{- printf "%s-%s" .Chart.Name "deployment" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.service" -}}
{{- printf "%s-%s" .Chart.Name "service" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.monitor" -}}
{{- printf "%s-%s" .Chart.Name "monitor" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.monitor.job" -}}
{{- printf "%s-%s-job" .Chart.Name "monitor" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.migrate" -}}
{{- printf "%s-%s" .Chart.Name "migrate" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.migrate.job" -}}
{{- printf "%s-%s-%s" .Chart.Name "migrate" "job" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.test" -}}
{{- printf "%s-%s" .Chart.Name "test" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.test.job" -}}
{{- printf "%s-%s-%s" .Chart.Name "test" "job" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.test.hostService" -}}
{{- printf "%s.%s.svc.cluster.local:%s" (include "crud-rest-api.name" .) .Release.Name .Values.service.port | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "crud-rest-api.selectorLabels" -}}
app.kubernetes.io/name: {{ include "crud-rest-api.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "crud-rest-api.labels" -}}
helm.sh/chart: {{ include "crud-rest-api.chart" . }}
{{ include "crud-rest-api.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "postgresql.fullname" -}}
{{- printf "%s-%s" .Release.Name "postgresql" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
