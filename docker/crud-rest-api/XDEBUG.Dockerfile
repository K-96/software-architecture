FROM crud-rest-api:latest

RUN apt-get update && apt-get install -y wget git unzip \
    && pecl install xdebug-2.9.6 \
    && docker-php-ext-enable xdebug

ADD ./xdebug.ini  /usr/local/etc/php/php.ini

WORKDIR /app

EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000", "-t", "public" ]