FROM postman/newman:latest
RUN mkdir /tests
COPY ./crud-rest-api-test-products-resources.postman_collection.json /tests/
WORKDIR /tests